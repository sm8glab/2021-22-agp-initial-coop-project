// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TP_Network : ModuleRules
{
	public TP_Network(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
