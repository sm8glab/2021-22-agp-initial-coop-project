// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TP_NetworkGameMode.generated.h"

UCLASS(minimalapi)
class ATP_NetworkGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATP_NetworkGameMode();
};



