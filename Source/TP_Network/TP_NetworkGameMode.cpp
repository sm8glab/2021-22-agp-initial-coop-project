// Copyright Epic Games, Inc. All Rights Reserved.

#include "TP_NetworkGameMode.h"
#include "TP_NetworkCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATP_NetworkGameMode::ATP_NetworkGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
