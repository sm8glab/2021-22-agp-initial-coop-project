v4.26 Third Person C++ Template project created

Changes:
 - Using Edit, Project settings, Input, Action mappings
	+ remove from Jump: OculusGo_left_Trigger_click
	+ remove from ResetVR: OculusGo_left_Trackpad_click

 - Assets from free Infinity blade Props asset pack, on Epic store, added
 - 4 PNGs of simple colours added